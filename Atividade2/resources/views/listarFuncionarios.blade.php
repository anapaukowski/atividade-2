<html>
    <body>
        <h1>Listar Funcionários</h1>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Cargo</th>
                    <th>Salario</th>
                    <th>Data de Nascimento</th>
                    <th>Departamento</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($funcionarios as $funcionario)
                <tr>
                    <td> {{ $funcionario->id }}</td>
                    <td> {{ $funcionario->nome}} </td>
                    <td> {{ $funcionario->cargo}} </td>
                    <td> {{ $funcionario->salario}} </td>
                    <td> {{ $funcionario->data_nascimento}} </td>
                    <td> {{ $funcionario->status}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @error('erro')
            <div>{{ $message }}
        @enderror
    </body>
</html>