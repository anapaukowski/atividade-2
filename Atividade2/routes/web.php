<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/listar-funcionarios', [App\Http\Controllers\FuncionariosController::class, 'listar']);

Route::get('/selecionar/{id}', [App\Http\Controllers\FuncionariosController::class, 'listarID']);

