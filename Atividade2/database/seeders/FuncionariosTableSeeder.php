<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FuncionariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('funcionarios')->insert([
            [
                'nome' => 'Stephani Borges Salgado',
                'cargo' => 'CEO',
                'salario' => 1.420,
                'data_nascimento' => '2007-07-13',
                'departamento' => 'Gerência',
                'status' => 1
            ],
            [
                'nome' => 'Ye',
                'cargo' => 'Gênio',
                'salario' => 500.000,
                'data_nascimento' => '1977-06-08',
                'departamento' => 'Música',
                'status' => 0
            ],
            [
                'nome' => 'Chico Moedas',
                'cargo' => 'Aquariano Nato',
                'salario' => 10.000,
                'data_nascimento' => '1996-05-06',
                'departamento' => 'Relacionamento',
                'status' => 0
            ],
            [
                'nome' => 'Rita Lee',
                'cargo' => 'Cantora',
                'salario' => 900.000,
                'data_nascimento' => '1947-12-31',
                'departamento' => 'Música',
                'status' => 0
            ],
            [
                'nome' => 'Priscila Evellyn',
                'cargo' => 'Tiriça',
                'salario' => 8.000,
                'data_nascimento' => '1999-07-14',
                'departamento' => 'Personalidade',
                'status' => 1
            ],
            [
                'nome' => 'Pedro Henrique',
                'cargo' => 'Reizinho',
                'salario' => 0.0,
                'data_nascimento' => '2023-08-31',
                'departamento' => 'Casa da Bebel',
                'status' => 0
            ],
        ]);
    }
}
