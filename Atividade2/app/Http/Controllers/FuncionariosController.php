<?php

namespace App\Http\Controllers;

use App\Models\Funcionario;
use Illuminate\Http\Request;

class FuncionariosController extends Controller
{
    public function listar()
    {
        $funcionarios = Funcionario::all();
        return view ('listarFuncionarios')->with('funcionarios', $funcionarios);
    }
    
    public function listarID($id)
    {
        $funcionarios = Funcionario::find($id);
        if ($funcionarios){
            return view('listarID')->with('funcionarios', $funcionarios);
        } else {
            return redirect('/listar-funcionarios')->withErrors(['erro' => 'Funcionário não encontrado']);
        }
    }
}
